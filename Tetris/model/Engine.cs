﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Tetris.objects;
using Tetris.objects.figures;

namespace Tetris.model
{
    public class Engine
    {
        private const int MaxX = 200;
        private const int MaxY = 600;
        private const int Blocksize = 20;
        private static int _points;
        private static int _time;
        private static int _lvl = 10;
        private static bool _end;
        private static bool _hide = true;
        private Figure _activeFigure;
        private Graphics _g2;
        private Playground _pg;
        private Figure _preparedFigure;
        private List<Block> _trash;

        public Engine()
        {
            this._pg = new Playground(this.GetMaxX(), this.GetMaxY(), Blocksize);
            this._trash = new List<Block>();
            this._activeFigure = this.GenFigure();
            this._preparedFigure = this.GenFigure();
            this._pg.AddFigure(this._activeFigure);
        }

        private void CheckGameEnd()
        {
            _end = this._trash.Any(b => b.Y1 <= 80);
        }

        private Block CheckNDown(Block b)
        {
            var sur = this._pg.GetNeighbours(b);
            return sur[1];
        }

        private Block CheckNLeft(Block b)
        {
            var sur = this._pg.GetNeighbours(b);
            return sur[0];
        }

        private Block CheckNRight(Block b)
        {
            var sur = this._pg.GetNeighbours(b);
            return sur[2];
        }

        public Block CheckNeighbour(Block b, char c)
        {
            Block ret = null;
            switch (c)
            {
                case 'l':
                    ret = this.CheckNLeft(b);
                    break;
                case 'r':
                    ret = this.CheckNRight(b);
                    break;
                case 'd':
                    ret = this.CheckNDown(b);
                    break;
            }
            return this._trash.Contains(ret) ? ret : null;
        }

        public bool DoLeft()
        {
            return this._activeFigure.Left();
        }

        public bool DoRight()
        {
            return this._activeFigure.Right();
        }

        public bool DoRotate()
        {
            return this._activeFigure.Rotate(this._trash);
        }

        public void DoStep()
        {
            this._activeFigure.Down();
        }

        public void FinishStep(Graphics graphicsObj)
        {
            this.TrashBlocks(this._pg.RemoveLines(this._trash));
            foreach (var b in this._trash)
            {
                b.Paint(graphicsObj);
            }
            this._activeFigure.Paint(graphicsObj);
            this.PaintPrepFig(graphicsObj);
            this.CheckGameEnd();
        }

        private static ColorType GenColor(Random random)
        {
            var rand = random.Next(1, 8);
            switch (rand)
            {
                case 1:
                    return ColorType.Orange;
                case 2:
                    return ColorType.Blue;
                case 3:
                    return ColorType.Cyan;
                case 4:
                    return ColorType.Green;
                case 5:
                    return ColorType.Magenta;
                case 6:
                    return ColorType.Red;
                case 7:
                    return ColorType.Yellow;
            }
            return ColorType.Black;
        }

        private Figure GenFigure()
        {
            var random = new Random();
            var rand = random.Next(1, 8);
            Figure ret = null;
            var color = GenColor(random);
            switch (rand)
            {
                case 1:
                    ret = new Square(color, Blocksize, this);
                    break;
                case 2:
                    ret = new I(color, Blocksize, this);
                    break;
                case 3:
                    ret = new L(color, Blocksize, this);
                    break;
                case 4:
                    ret = new J(color, Blocksize, this);
                    break;
                case 5:
                    ret = new S(color, Blocksize, this);
                    break;
                case 6:
                    ret = new Z(color, Blocksize, this);
                    break;
                case 7:
                    ret = new T(color, Blocksize, this);
                    break;
            }
            return ret;
        }

        public int GetBs()
        {
            return Blocksize;
        }

        public bool GetEnd()
        {
            return _end;
        }

        internal bool GetHide()
        {
            return _hide;
        }

        public int GetLvl()
        {
            return _lvl;
        }

        public int GetMaxX()
        {
            return MaxX;
        }

        public int GetMaxY()
        {
            return MaxY;
        }

        public int GetPoints()
        {
            return _points;
        }

        public string GetTime()
        {
            var elapsedTime = new DateTime(TimeSpan.FromSeconds(_time).Ticks);
            return elapsedTime.ToString("mm:ss");
        }

        internal void IncreaseTime()
        {
            _time++;
            var l = _time/60;
            _lvl = 10 - l;
        }

        internal void NotifyGameEnd()
        {
            _end = true;
        }

        internal void NotifyPosChange(Figure figure)
        {
            this._pg.UpdateFigure(figure);
        }

        private void PaintPrepFig(Graphics graphicsObj)
        {
            var tmp = this._preparedFigure;
            Figure paint;
            if (tmp is Square)
            {
                paint = new Square(ColorType.Black, Blocksize, this);
            }
            else if (tmp is I)
            {
                paint = new I(ColorType.Black, Blocksize, this);
            }
            else if (tmp is L)
            {
                paint = new L(ColorType.Black, Blocksize, this);
            }
            else if (tmp is J)
            {
                paint = new J(ColorType.Black, Blocksize, this);
            }
            else if (tmp is S)
            {
                paint = new S(ColorType.Black, Blocksize, this);
            }
            else if (tmp is Z)
            {
                paint = new Z(ColorType.Black, Blocksize, this);
            }
            else
            {
                paint = new T(ColorType.Black, Blocksize, this);
            }
            paint.PosChangePrepPaint(50, 715);
            paint.Paint(graphicsObj);
        }

        internal void Reset()
        {
            _points = 0;
            _time = 0;
            _lvl = 10;
            _end = false;
            _hide = true;
            this._pg = new Playground(this.GetMaxX(), this.GetMaxY(), Blocksize);
            this._trash = new List<Block>();
            this._activeFigure = this.GenFigure();
            this._preparedFigure = this.GenFigure();
            this._pg.AddFigure(this._activeFigure);
        }

        internal void SetGraphics(Graphics graphicsObj)
        {
            this._g2 = graphicsObj;
        }

        private void TrashBlocks(ICollection<Block> linkedList)
        {
            var multi = linkedList.Count/10;
            var newPoints = linkedList.Count*multi*(11 - _lvl);
            if (newPoints > 0)
            {
                _points += newPoints;
            }
            foreach (var b in linkedList)
            {
                this._trash.Remove(b);
            }
        }

        internal void TrashMe(Figure figure)
        {
            foreach (var b in figure.Blocks)
            {
                this._trash.Add(b);
            }
            this._activeFigure = this._preparedFigure;
            this._preparedFigure = this.GenFigure();
            this.PaintPrepFig(this._g2);
            this._pg.AddFigure(this._activeFigure);
        }
    }
}