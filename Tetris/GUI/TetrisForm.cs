﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using Tetris.model;
using Tetris.Properties;

namespace Tetris.GUI
{
    public partial class TetrisForm : Form
    {
        private const int TimerLevelMultiplicator = 50;
        private readonly Engine _engine;
        private Bitmap _bmp;
        private bool _endMsgFlag;
        private Graphics _g, _g2;
        private Graphics _graphicsObj;

        public TetrisForm(Engine engine)
        {
            this.InitializeComponent();
            this._engine = engine;
            this.SetStyle(
                          ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint | ControlStyles.OptimizedDoubleBuffer, true);
            this._bmp = new Bitmap(250, 800);
            this._g = Graphics.FromImage(this._bmp);
            this._graphicsObj = this._g;
            this._drawBorderLines();
            engine.SetGraphics(this._g2);
            this._graphicsObj.CompositingQuality = CompositingQuality.HighSpeed;
        }

        private void ExitToolStripMenuItemClick1(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void NewGameToolStripMenuItemClick(object sender, EventArgs e)
        {
            this._engine.Reset();
            this.timer1.Stop();
            this.timer2.Stop();
            this.timer1.Start();
            this.timer2.Start();
            this.Refresh();
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            switch (keyData)
            {
                case Keys.Up:
                    this.TetrisFormKeyDown(null, new KeyEventArgs(Keys.W));
                    break;
                case Keys.Down:
                    this.TetrisFormKeyDown(null, new KeyEventArgs(Keys.S));
                    break;
                case Keys.Left:
                    this.TetrisFormKeyDown(null, new KeyEventArgs(Keys.A));
                    break;
                case Keys.Right:
                    this.TetrisFormKeyDown(null, new KeyEventArgs(Keys.D));
                    break;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void RotateButtonClick(object sender, EventArgs e)
        {
            this._engine.DoRotate();
            this.Refresh();
        }

        private void StepDownButtonClick(object sender, EventArgs e)
        {
            this._engine.DoStep();
            this.Refresh();
        }

        private void StepLeftButtonClick(object sender, EventArgs e)
        {
            this._engine.DoLeft();
            this.Refresh();
        }

        private void StepRightButtonClick(object sender, EventArgs e)
        {
            this._engine.DoRight();
            this.Refresh();
        }

        private void TetrisFormKeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.W:

                {
                    this.RotateButtonClick(null, null);
                }
                    break;
                case Keys.A:
                case Keys.Left:
                {
                    this.StepLeftButtonClick(null, null);
                }
                    break;
                case Keys.D:
                case Keys.Right:
                {
                    this.StepRightButtonClick(null, null);
                }
                    break;
                case Keys.S:
                case Keys.Down:
                {
                    this.StepDownButtonClick(null, null);
                }
                    break;
                default:
                {
                    e.SuppressKeyPress = true;
                    return;
                }
            }
            e.Handled = true;
        }

        private void TetrisFormPaint(object sender, PaintEventArgs e)
        {
            if (this._engine.GetEnd())
            {
                this.timer1.Stop();
                this.timer2.Stop();
                if (this._endMsgFlag)
                {
                    return;
                }
                MessageBox.Show("" + this._engine.GetPoints() + Resources.TetrisForm_TetrisFormPaint__Points, Resources.TetrisForm_TetrisFormPaint_Game_Over);
                this._endMsgFlag = true;
            }
            else
            {
                this._endMsgFlag = false;
                this._bmp = new Bitmap(250, 800);
                this._g = Graphics.FromImage(this._bmp);
                this._g2 = this.CreateGraphics();
                this._graphicsObj = this._g;
                this._engine.SetGraphics(this._g2);
                this._engine.FinishStep(this._graphicsObj);
                this._drawBorderLines();
                this.label2.Text = "" + this._engine.GetPoints();
                this.label4.Text = "" + (11 - this._engine.GetLvl());
                this.label6.Text = "" + this._engine.GetTime();
                e.Graphics.DrawImage(this._bmp, new Point(0, 0));
            }
        }

        private void Timer1Tick(object sender, EventArgs e)
        {
            this.StepDownButtonClick(sender, e);
        }

        private void Timer2Tick(object sender, EventArgs e)
        {
            this._engine.IncreaseTime();
            this.timer1.Interval = this._engine.GetLvl()*TimerLevelMultiplicator;
        }

        private void _drawBorderLines()
        {
            var myPen = new Pen(Color.Red, 5);
            this._graphicsObj.DrawLine(myPen, 0, 600, 200, 600);
            this._graphicsObj.DrawLine(myPen, 0, 80, 200, 80);
            this._graphicsObj.DrawLine(myPen, 200, 603, 200, 78);
        }
    }
}