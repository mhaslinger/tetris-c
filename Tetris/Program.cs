﻿using System;
using System.Windows.Forms;
using Tetris.GUI;
using Tetris.model;

namespace at.marce.Tetris
{
    internal static class Program
    {
        /// <summary>
        ///     Der Haupteinstiegspunkt für die Anwendung.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            var engine = new Engine();
            Application.Run(new TetrisForm(engine));
        }
    }
}