﻿namespace Tetris.objects
{
    public enum ColorType
    {
        Blue,
        Red,
        Green,
        Yellow,
        Magenta,
        Black,
        Cyan,
        Orange
    }
}