﻿using System.Drawing;

namespace Tetris.objects
{
    public class Block
    {
        private readonly ColorType _color;
        private int[] _id;
        private int _x1, _x2, _y1, _y2;

        public int X1
        {
            get { return this._x1; }
            set { this._x1 = value; }
        }

        public int X2
        {
            get { return this._x2; }
            set { this._x2 = value; }
        }

        public int Y1
        {
            get { return this._y1; }
            set { this._y1 = value; }
        }

        public int Y2
        {
            get { return this._y2; }
            set { this._y2 = value; }
        }

        public Block(int x1, int y1, int x2, int y2, ColorType color)
        {
            this._x1 = x1;
            this._x2 = x2;
            this._y1 = y1;
            this._y2 = y2;
            this._color = color;
            this._id = null;
        }

        public void Fall(int bs)
        {
            this._y1 = this._y1 + bs;
            this._y2 = this._y2 + bs;
        }

        private Color GetColor()
        {
            var ret = Color.Black;
            switch (this._color)
            {
                case ColorType.Blue:
                    ret = Color.Blue;
                    break;
                case ColorType.Red:
                    ret = Color.Red;
                    break;
                case ColorType.Yellow:
                    ret = Color.Yellow;
                    break;
                case ColorType.Green:
                    ret = Color.Green;
                    break;
                case ColorType.Black:
                    ret = Color.Black;
                    break;
                case ColorType.Cyan:
                    ret = Color.Cyan;
                    break;
                case ColorType.Magenta:
                    ret = Color.Magenta;
                    break;
                case ColorType.Orange:
                    ret = Color.DarkOrange;
                    break;
            }
            return ret;
        }

        public int[] GetPlaygroundId()
        {
            return this._id;
        }

        internal bool IsInFrame(ColFrame colFrame)
        {
            var b = this;
            var c = colFrame;
            return ((b._x1 >= c.X1) && (b._x2 <= c.X2)) && ((b._y1 <= c.Y1) && (b._y2 >= c.Y2));
        }

        public void Paint(Graphics graphicsObj)
        {
            var myPen1 = new Pen(Color.Gray, 1);
            var brush = new SolidBrush(this.GetColor());
            var myRectangle = new Rectangle(this._x1, this._y1, 20, 20);
            graphicsObj.FillRectangle(brush, myRectangle);
            graphicsObj.DrawRectangle(myPen1, myRectangle);
        }

        public void SavePlaygroundId(int[] id)
        {
            this._id = id;
        }
    }
}