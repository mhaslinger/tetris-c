﻿using System.Collections.Generic;
using System.Linq;

namespace Tetris.objects
{
    internal class ColFrame
    {
        internal int X1, X2, Y1, Y2;

        public ColFrame(int x1, int y1, int x2, int y2)
        {
            this.X1 = x1;
            this.X2 = x2;
            this.Y1 = y1;
            this.Y2 = y2;
        }

        public bool CheckColRotate(List<Block> blocks, int maxX, int maxY)
        {
            if (this.X1 < 0 || this.X2 > maxX || this.Y1 > maxY)
            {
                return false;
            }
            return blocks.All(b => !b.IsInFrame(this));
        }

        internal void ShiftDown()
        {
            this.Y1 += 20;
            this.Y2 += 20;
        }

        internal void ShiftLeft()
        {
            this.X1 -= 20;
            this.X2 -= 20;
        }

        internal void ShiftRight()
        {
            this.X1 += 20;
            this.X2 += 20;
        }
    }
}