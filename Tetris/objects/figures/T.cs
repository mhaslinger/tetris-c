﻿using Tetris.model;

namespace Tetris.objects.figures
{
    internal class T : Figure
    {
        private Block _b1, _b2, _b3, _b4;
        private int _rotpos;

        public T(ColorType color, int blocksize, Engine e)
            : base(color, blocksize, e)
        {
            var x1 = 4*e.GetBs();
            var y1 = 2*e.GetBs();
            var bs = e.GetBs();
            this.Colframe = new ColFrame(x1 - bs, y1 + bs, x1 + 2*bs, y1 - 2*bs);
            this._b1 = new Block(x1, y1, x1 + bs, y1 - bs, color);
            this._b2 = new Block(x1 - bs, y1, x1, y1 - bs, color);
            this._b3 = new Block(x1, y1 - bs, x1 + bs, y1 - 2*bs, color);
            this._b4 = new Block(x1 + bs, y1, x1 + 2*bs, y1 - bs, color);
            this.Blocks = new Block[4];
            this.Blocks[0] = this._b1;
            this.Blocks[1] = this._b2;
            this.Blocks[2] = this._b3;
            this.Blocks[3] = this._b4;
            this._rotpos = 0;
        }

        public override void DoRotate()
        {
            var b2 = this.Blocks[1];
            var b3 = this.Blocks[2];
            var b4 = this.Blocks[3];
            switch (this._rotpos)
            {
                case 0:
                    b2.X1 += this.Blocksize;
                    b2.X2 += this.Blocksize;
                    b2.Y1 -= this.Blocksize;
                    b2.Y2 -= this.Blocksize;
                    b3.X1 += this.Blocksize;
                    b3.X2 += this.Blocksize;
                    b3.Y1 += this.Blocksize;
                    b3.Y2 += this.Blocksize;
                    b4.X1 -= this.Blocksize;
                    b4.X2 -= this.Blocksize;
                    b4.Y1 += this.Blocksize;
                    b4.Y2 += this.Blocksize;
                    this._rotpos = 1;
                    break;
                case 1:
                    b2.X1 += this.Blocksize;
                    b2.X2 += this.Blocksize;
                    b2.Y1 += this.Blocksize;
                    b2.Y2 += this.Blocksize;
                    b3.X1 -= this.Blocksize;
                    b3.X2 -= this.Blocksize;
                    b3.Y1 += this.Blocksize;
                    b3.Y2 += this.Blocksize;
                    b4.X1 -= this.Blocksize;
                    b4.X2 -= this.Blocksize;
                    b4.Y1 -= this.Blocksize;
                    b4.Y2 -= this.Blocksize;
                    this._rotpos = 2;
                    break;
                case 2:
                    b2.X1 -= this.Blocksize;
                    b2.X2 -= this.Blocksize;
                    b2.Y1 += this.Blocksize;
                    b2.Y2 += this.Blocksize;
                    b3.X1 -= this.Blocksize;
                    b3.X2 -= this.Blocksize;
                    b3.Y1 -= this.Blocksize;
                    b3.Y2 -= this.Blocksize;
                    b4.X1 += this.Blocksize;
                    b4.X2 += this.Blocksize;
                    b4.Y1 -= this.Blocksize;
                    b4.Y2 -= this.Blocksize;
                    this._rotpos = 3;
                    break;
                case 3:
                    b2.X1 -= this.Blocksize;
                    b2.X2 -= this.Blocksize;
                    b2.Y1 -= this.Blocksize;
                    b2.Y2 -= this.Blocksize;
                    b3.X1 += this.Blocksize;
                    b3.X2 += this.Blocksize;
                    b3.Y1 -= this.Blocksize;
                    b3.Y2 -= this.Blocksize;
                    b4.X1 += this.Blocksize;
                    b4.X2 += this.Blocksize;
                    b4.Y1 += this.Blocksize;
                    b4.Y2 += this.Blocksize;
                    this._rotpos = 0;
                    break;
            }
            this.Engine.NotifyPosChange(this);
        }

        public override void PosChangePrepPaint(int x1N, int y1N)
        {
            this.X1 = x1N;
            this.Y1 = y1N;
            const int bs = 20;
            this._b1 = new Block(this.X1, this.Y1, this.X1 + bs, this.Y1 - bs, this.Color);
            this._b2 = new Block(this.X1 - bs, this.Y1, this.X1, this.Y1 - bs, this.Color);
            this._b3 = new Block(this.X1, this.Y1 - bs, this.X1 + bs, this.Y1 - 2*bs, this.Color);
            this._b4 = new Block(this.X1 + bs, this.Y1, this.X1 + 2*bs, this.Y1 - bs, this.Color);
            this.Blocks = new Block[4];
            this.Blocks[0] = this._b1;
            this.Blocks[1] = this._b2;
            this.Blocks[2] = this._b3;
            this.Blocks[3] = this._b4;
        }
    }
}