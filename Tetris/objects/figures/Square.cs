﻿using Tetris.model;

namespace Tetris.objects.figures
{
    internal class Square : Figure
    {
        public Square(ColorType color, int blocksize, Engine e)
            : base(color, blocksize, e)
        {
            var x1 = 4*e.GetBs();
            var y1 = 2*e.GetBs();
            var x2 = x1 + e.GetBs()*2;
            const int y2 = 0;
            var bs = e.GetBs();
            this.Colframe = new ColFrame(x1 - bs, y1 + bs, x2 + bs, y2 + bs);
            var b1 = new Block(x1, y1, x1 + bs, y1 - bs, color);
            var b2 = new Block(x1 + bs, y1, x2, y1 - bs, color);
            var b3 = new Block(x1, y1 - bs, x1 + bs, y2, color);
            var b4 = new Block(x2 - bs, y2 + bs, x2, y2, color);
            this.Blocks = new Block[4];
            this.Blocks[0] = b1;
            this.Blocks[1] = b2;
            this.Blocks[2] = b3;
            this.Blocks[3] = b4;
        }

        public override void DoRotate()
        {
            //skip for square
        }

        public override void PosChangePrepPaint(int x1N, int y1N)
        {
            this.X1 = x1N;
            this.Y1 = y1N;
            this.X2 = x1N + 40;
            this.Y2 = y1N - 40;
            const int bs = 20;
            var b1 = new Block(this.X1, this.Y1, this.X1 + bs, this.Y1 - bs, this.Color);
            var b2 = new Block(this.X1 + bs, this.Y1, this.X2, this.Y1 - bs, this.Color);
            var b3 = new Block(this.X1, this.Y1 - bs, this.X1 + bs, this.Y2, this.Color);
            var b4 = new Block(this.X2 - bs, this.Y2 + bs, this.X2, this.Y2, this.Color);
            this.Blocks = new Block[4];
            this.Blocks[0] = b1;
            this.Blocks[1] = b2;
            this.Blocks[2] = b3;
            this.Blocks[3] = b4;
        }
    }
}