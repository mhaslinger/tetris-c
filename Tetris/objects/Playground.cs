﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tetris.Properties;

namespace Tetris.objects
{
    internal class Playground
    {
        private readonly Block[,] _arr;
        private readonly int _bs;
        private readonly int _maxY;
        private readonly int _x;
        private readonly int _y;

        public Playground(int x, int y, int bs)
        {
            var height = y/bs;
            var width = x/bs;
            this._arr = new Block[height, width];
            this._bs = bs;
            this._x = width;
            this._y = height;
            this._maxY = y;
        }

        public void AddBlock(Block b)
        {
            var xVal = (b.X1/this._bs);
            var yVal = (b.Y1/this._bs);
            this._arr[yVal, xVal] = b;
            int[] id = {yVal, xVal};
            b.SavePlaygroundId(id);
        }

        internal void AddFigure(Figure activeFigure)
        {
            foreach (var b in activeFigure.GetBlocks())
            {
                this.AddBlock(b);
            }
        }

        internal void ConsoleOut()
        {
            Console.WriteLine(Resources.Playground_ConsoleOut_Playground_Output);
            for (var i = 0; i < 30; i++)
            {
                for (var j = 0; j < 10; j++)
                {
                    Console.Write(this._arr[i, j] != null ? "X" : "-");
                }
                Console.WriteLine();
            }
        }

        public Block[] GetNeighbours(Block b)
        {
            var id = b.GetPlaygroundId();
            var yVal = id[0];
            var xVal = id[1];
            Block[] ret = {null, null, null};
            if (xVal != 0)
            {
                ret[0] = this._arr[yVal, xVal - 1];
            }
            if (yVal < this._y - 1)
            {
                ret[1] = this._arr[yVal + 1, xVal];
            }
            if (xVal < this._x - 1)
            {
                ret[2] = this._arr[yVal, xVal + 1];
            }
            return ret;
        }

        public List<Block> RemoveLines(List<Block> trashList)
        {
            var fullList = new List<List<Block>>();
            for (var i = this._y - 1; i >= 0; i--)
            {
                var tmpList = new List<Block>();
                for (var j = 0; j < this._x; j++)
                {
                    if (this._arr[i, j] == null)
                    {
                        tmpList = null;
                        break;
                    }
                    if (trashList.Contains(this._arr[i, j]))
                    {
                        tmpList.Add(this._arr[i, j]);
                    }
                    else
                    {
                        tmpList = null;
                        break;
                    }
                }
                if (tmpList == null)
                {
                    continue;
                }
                fullList.Add(tmpList);
                this._removeBlocks(tmpList, trashList);
                i = this._y;
            }
            return fullList.SelectMany(l => l).ToList();
        }

        public void UpdateBlock(Block b)
        {
            var id = b.GetPlaygroundId();
            this._arr[id[0], id[1]] = null;
            this.AddBlock(b);
        }

        internal void UpdateFigure(Figure figure)
        {
            var tmp = new Block[4];
            Array.Copy(figure.Blocks, tmp, 4);
            Array.Sort(tmp, (b1, b2) => -b1.Y1.CompareTo(b2.Y1));
            foreach (var b in tmp)
            {
                this.UpdateBlock(b);
            }
        }

        private void _drop(int yVal, int xVal, int bs)
        {
            var tmp = this._arr[yVal, xVal];
            tmp.Fall(bs);
            if (tmp.Y1 > this._maxY)
            {
                this._arr[yVal, xVal] = null;
            }
            this.UpdateBlock(tmp);
            if (this._arr[yVal - 1, xVal] != null)
            {
                this._drop(yVal - 1, xVal, bs);
            }
        }

        private void _removeBlocks(IEnumerable<Block> remList, List<Block> trashList)
        {
            foreach (var b in remList)
            {
                var id = b.GetPlaygroundId();
                var yVal = id[0];
                var xVal = id[1];
                this._arr[yVal, xVal] = null;
                var allTrashAbove = new List<Block>(this._y - yVal);
                for (var i = yVal - 1; i >= 0; i--)
                {
                    if (this._arr[i, xVal] == null)
                    {
                        continue;
                    }
                    if (trashList.Contains(this._arr[i, xVal]))
                    {
                        allTrashAbove.Add(this._arr[i, xVal]);
                    }
                }
                foreach (var block in allTrashAbove.OrderByDescending(tempBlock => tempBlock.Y1))
                {
                    block.Fall(this._bs);
                    this.UpdateBlock(block);
                }
            }
        }
    }
}