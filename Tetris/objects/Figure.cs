﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Tetris.model;

namespace Tetris.objects
{
    internal abstract class Figure
    {
        internal Block[] Blocks;
        internal int Blocksize;
        internal ColFrame Colframe;
        internal ColorType Color;
        internal Engine Engine;
        internal int MaxX, MaxY;
        internal int X1, X2, Y1, Y2;
        private bool _terminate;

        protected Figure(ColorType color, int blocksize, Engine e)
        {
            this.Color = color;
            this.Blocksize = blocksize;
            this.Engine = e;
            this.MaxX = this.Engine.GetMaxX();
            this.MaxY = this.Engine.GetMaxY();
            this._terminate = false;
            this.X1 = 0;
            this.X2 = 0;
            this.Y1 = 0;
            this.Y2 = 0;
        }

        private bool CheckBlockFamily(Block a, Block b)
        {
            return this.Blocks.Contains(a) && this.Blocks.Contains(b);
        }

        private void CheckCollision(Block block)
        {
            var n = this.Engine.CheckNeighbour(block, 'd');
            if (n != null)
            {
                this._terminate = true;
                if (n.Y2 == 80)
                {
                    this.Engine.NotifyGameEnd();
                }
            }
            if (block.Y1 == this.MaxY)
            {
                this._terminate = true;
            }
        }

        private bool CheckLeft()
        {
            return !(from block in this.Blocks
                     let n = this.Engine.CheckNeighbour(block, 'l')
                     where n != null
                     where !this.CheckBlockFamily(block, n)
                     select block).Any();
        }

        private bool CheckRight()
        {
            return !(from block in this.Blocks
                     let n = this.Engine.CheckNeighbour(block, 'r')
                     where n != null
                     where !this.CheckBlockFamily(block, n)
                     select block).Any();
        }

        private void CheckTerminate()
        {
            if (this._terminate)
            {
                this.Engine.TrashMe(this);
            }
        }

        public abstract void DoRotate();

        public bool Down()
        {
            foreach (var b in this.Blocks.Where(b => b.Y1 == this.MaxY))
            {
                this._terminate = true;
            }
            foreach (var block in this.Blocks)
            {
                if (!this.PossibleDownShift(this.Blocksize, block.Y1))
                {
                    this.CheckTerminate();
                    return false;
                }
                var n = this.Engine.CheckNeighbour(block, 'd');
                if (n == null)
                {
                    continue;
                }
                if (this.CheckBlockFamily(block, n))
                {
                    continue;
                }
                this._terminate = true;
                return false;
            }
            foreach (var block in this.Blocks)
            {
                this.ShiftBlockDown(block);
            }
            this.Y1 += this.Blocksize;
            this.Y2 += this.Blocksize;
            this.UpdateCf('d');
            this.NotifyEngine();
            this.CheckTerminate();
            return true;
        }

        public Block[] GetBlocks()
        {
            return this.Blocks;
        }

        public bool Left()
        {
            if (this.Blocks.Any(block => !this.PossibleShift(this.Blocksize, block.X1)))
            {
                return false;
            }
            if (!this.CheckLeft())
            {
                return false;
            }
            foreach (var block in this.Blocks)
            {
                this.ShiftBlockLeft(block);
            }
            this.X1 -= this.Blocksize;
            this.X2 -= this.Blocksize;
            this.UpdateCf('l');
            this.NotifyEngine();
            return true;
        }

        private void NotifyEngine()
        {
            this.Engine.NotifyPosChange(this);
        }

        public void Paint(Graphics graphicsObj)
        {
            foreach (var b in this.Blocks)
            {
                b.Paint(graphicsObj);
            }
        }

        public abstract void PosChangePrepPaint(int x1, int y1);

        private bool PossibleDownShift(int bs, int y)
        {
            if (this._terminate)
            {
                return false;
            }
            if ((y + bs) >= this.MaxY)
            {
                this._terminate = true;
                return false;
            }
            ;
            return true;
        }

        private bool PossibleShift(int bs, int x)
        {
            if (x <= 0)
            {
                return false;
            }
            var val = this.MaxX + bs;
            if (val < 0)
            {
                return false;
            }
            if ((x + bs) > this.MaxX)
            {
                return false;
            }
            return x - bs >= 0;
        }

        public bool Right()
        {
            if (this.Blocks.Any(block => !this.PossibleShift(this.Blocksize, block.X2)))
            {
                return false;
            }
            if (!this.CheckRight())
            {
                return false;
            }
            foreach (var block in this.Blocks)
            {
                this.ShiftBlockRight(block);
            }
            this.X1 += this.Blocksize;
            this.X2 += this.Blocksize;
            this.UpdateCf('r');
            this.NotifyEngine();
            return true;
        }

        public bool Rotate(List<Block> blocks)
        {
            if (!this.Colframe.CheckColRotate(blocks, this.Engine.GetMaxX(), this.Engine.GetMaxY()))
            {
                return false;
            }
            this.DoRotate();
            return true;
        }

        private void ShiftBlockDown(Object b)
        {
            var block = (Block) b;
            var n = this.Engine.CheckNeighbour(block, 'd');
            this.CheckCollision(block);
            if ((n != null && !this.CheckBlockFamily(block, n)) || this._terminate)
            {
                return;
            }
            block.Y1 = block.Y1 + this.Blocksize;
            block.Y2 = block.Y2 + this.Blocksize;
        }

        private void ShiftBlockLeft(Object b)
        {
            var block = (Block) b;
            var n = this.Engine.CheckNeighbour(block, 'l');
            if (n != null && !this.CheckBlockFamily(block, n))
            {
                return;
            }
            block.X1 = block.X1 - this.Blocksize;
            block.X2 = block.X2 - this.Blocksize;
        }

        private void ShiftBlockRight(Object b)
        {
            var block = (Block) b;
            var n = this.Engine.CheckNeighbour(block, 'r');
            if (n != null && !this.CheckBlockFamily(block, n))
            {
                return;
            }
            block.X1 = block.X1 + this.Blocksize;
            block.X2 = block.X2 + this.Blocksize;
        }

        public void UpdateCf(char c)
        {
            switch (c)
            {
                case 'l':
                    this.Colframe.ShiftLeft();
                    return;
                case 'r':
                    this.Colframe.ShiftRight();
                    return;
                case 'd':
                    this.Colframe.ShiftDown();
                    return;
            }
        }
    }
}